const mainColors = {
  green1: '#0BCAD4',
  green2: '#EDFCFD',
  green3: '#26C281',
  blue: '#0066CB',
  dark1: '#112340',
  dark2: '#495A75',
  grey1: '#7D8797',
  grey2: '#E9E9E9',
  grey3: '#8092AF',
  grey4: '#EDEEF0',
  grey5: '#B1B7C2',
  blue1: '#0066CB',
  black1: '#000000',
  black2: 'rgba(0,0,0,0.5)',
  red1: '#C3272B',
};

export const colors = {
  primary: mainColors.green1,
  secondary: mainColors.dark1,
  tertiary: mainColors.blue1,
  chat: mainColors.grey3,
  input: mainColors.grey4,
  white: 'white',
  black: 'black',
  message: {
    light: mainColors.green2,
    dark: mainColors.green1,
  },
  text: {
    primary: mainColors.dark1,
    secondary: mainColors.grey1,
    menuActive: mainColors.green1,
    menuInactive: mainColors.dark2,
  },
  send: mainColors.blue,
  button: {
    primary: {
      background: mainColors.green1,
      text: 'white',
    },
    secondary: {
      background: 'white',
      text: mainColors.dark1,
    },
    disable: {
      background: mainColors.grey4,
      text: mainColors.grey5,
    },
  },
  border: mainColors.grey2,
  backgroundList: mainColors.green2,
  loadingBackground: mainColors.black2,
  error: mainColors.red1,
  success: mainColors.green3,
};
