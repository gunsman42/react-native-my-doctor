import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {launchImageLibrary} from 'react-native-image-picker';
import {ILNullPhoto} from '../../assets';
import {Button, Gap, Header, Input} from '../../components';
import Profile from '../../components/molecules/Prorfile';
import {Firebase} from '../../config';
import {colors, getData, showError, storeData} from '../../utils';

const UpdateProfile = ({navigation}) => {
  const [profile, setProfile] = useState({
    photo: ILNullPhoto,
    fullName: '',
    profession: '',
    email: '',
  });

  const [password, setPassword] = useState('');
  const [photo, setPhoto] = useState(ILNullPhoto);
  const [photoForDB, setPhotoForDB] = useState('');

  useEffect(() => {
    getData('user').then((res) => {
      const data = res;
      console.log(data);
      setPhoto({uri: res.photo});

      setProfile(data);
    });
  }, []);

  const updateProfile = () => {
    console.log('profile: ', profile);
    // navigation.goBack('UserProfile');
    console.log('new Password: ', password);

    if (password.length > 0) {
      if (password.length < 6) {
        showError('Your password must be 6 characters or longer');
      } else {
        //Update the Password
        updatePassword();
        updateProfileData();

        navigation.replace('MainApp');
      }
    } else {
      updateProfileData();
    }
  };

  const updatePassword = () => {
    Firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        //Update Password
        user.updatePassword(password).catch((err) => {
          showError(err.message);
        });
      }
    });
  };

  const updateProfileData = () => {
    const data = profile;
    data.photo = photoForDB;
    Firebase.database()
      .ref(`users/${profile.uid}/`)
      .update(data)
      .then(() => {
        console.log('Success', data);
        storeData('user', data);
        navigation.navigate('MainApp');
      })
      .catch((err) => {
        showError(err.message);
      });
  };

  const changeText = (key, value) => {
    setProfile({
      ...profile,
      [key]: value,
    });
  };

  const options = {
    includeBase64: true,
    quality: 0.5,
    maxWidth: 200,
    maxHeight: 200,
  };

  const editPhoto = () => {
    launchImageLibrary(options, (response) => {
      if (response.didCancel || response.error) {
        showError("Oops, you have'nt choose any photos");
      } else {
        setPhotoForDB(`data:${response.type};base64, ${response.base64}`);
        console.log('response Upload photo: ', response);
        const source = {uri: response.uri};
        setPhoto(source);
      }
    });
  };

  return (
    <View style={styles.page}>
      <Header title="Edit Profile" onPress={() => navigation.goBack()} />
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <View style={styles.photoProfile}>
          {profile.fullName.length > 0 && (
            <Profile photo={photo} isRemove onPress={editPhoto} />
          )}
        </View>
        <Input
          label="Full Name"
          value={profile.fullName}
          onChangeText={(value) => changeText('fullName', value)}
        />
        <Gap height={24} />
        <Input
          label="Pekerjaan"
          value={profile.profession}
          onChangeText={(value) => changeText('profession', value)}
        />
        <Gap height={24} />
        <Input label="Email Address" value={profile.email} disable />
        <Gap height={24} />
        <Input
          password
          label="Password"
          value={password}
          onChangeText={(value) => setPassword(value)}
        />
        <Gap height={24} />
        <Button title="Save Profile" onPress={updateProfile} />
      </ScrollView>
    </View>
  );
};

export default UpdateProfile;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
    paddingVertical: 16,
    marginTop: -16,
  },
  photoProfile: {
    paddingVertical: 16,
  },
  container: {
    paddingHorizontal: 40,
  },
});
