import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {Button, Gap, Header, Input, Loading} from '../../components';
import {Firebase} from '../../config';
import {colors, showError, storeData} from '../../utils';
import useForm from '../../utils/useForm';

const SignUp = ({navigation}) => {
  // const [fullName, setFullName] = useState('');
  // const [profession, setProfession] = useState('');
  // const [email, setEmail] = useState('');
  // const [password, setPassword] = useState('');

  const [form, setForm] = useForm({
    fullName: '',
    profession: '',
    email: '',
    password: '',
  });

  const [loading, setLoading] = useState(false);

  const onContinue = () => {
    console.log(form);

    setLoading(true);
    //Firebase Authitentication
    Firebase.auth()
      .createUserWithEmailAndPassword(form.email, form.password)

      .then((success) => {
        setForm('reset');
        //Save to Firebase Databse
        const data = {
          fullName: form.fullName,
          profession: form.profession,
          email: form.email,
          uid: success.user.uid,
        };
        //Save data to firebase
        Firebase.database()
          .ref('users/' + success.user.uid + '/')
          .set(data);

        storeData('user', data);

        // showMessage({
        //   message: 'Successfully registered',
        //   type: 'default',
        //   icon: 'success',
        //   backgroundColor: colors.success,
        //   color: colors.white,
        // });
        setLoading(false);
        navigation.navigate('UploadPhoto', data);
      })

      .catch((error) => {
        const errorMessage = error.message;
        setLoading(false);
        showError(errorMessage);
      });
  };
  return (
    <>
      <View style={styles.page}>
        <Header
          title="Daftar Akun"
          icon="back-dark"
          onPress={() => navigation.goBack()}
        />
        <View style={styles.container}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <Input
              label="Nama Lengkap"
              value={form.fullName}
              onChangeText={(input) => setForm('fullName', input)}
            />
            <Gap height={24} />
            <Input
              label="Pekerjaan"
              value={form.profession}
              onChangeText={(input) => setForm('profession', input)}
            />
            <Gap height={24} />
            <Input
              label="Email Address"
              value={form.email}
              onChangeText={(input) => setForm('email', input)}
            />
            <Gap height={24} />
            <Input
              label="Password"
              password
              value={form.password}
              onChangeText={(input) => setForm('password', input)}
            />
            <Gap height={40} />
            <Button
              title="Continue"
              // onPress={() => navigation.navigate('UploadPhoto')}
              onPress={onContinue}
            />
          </ScrollView>
        </View>
      </View>
      {loading && <Loading />}
    </>
  );
};

export default SignUp;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
  },
  container: {
    padding: 40,
    paddingTop: 0,
  },
});
