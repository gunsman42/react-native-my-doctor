import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {Header, List} from '../../components';
import {Firebase} from '../../config';
import {colors} from '../../utils';

const ChooseDoctor = ({navigation, route}) => {
  const [listDoctor, setListDoctor] = useState([]);
  useEffect(() => {
    callDoctorByCategory(itemCategory.category);
  }, []);

  const callDoctorByCategory = (category) => {
    Firebase.database()
      .ref('doctors/')
      .orderByChild('category')
      .equalTo(category)
      .once('value')
      .then((res) => {
        if (res.val()) {
          const oldData = res.val();
          const data = [];
          Object.keys(oldData).map((item) => {
            data.push({
              id: item,
              data: oldData[item],
            });
            console.log('data List Doctor: ', data);
            setListDoctor(data);
          });
        }
      });
  };
  const itemCategory = route.params;
  return (
    <View style={styles.page}>
      <Header
        title={`Pilih ${itemCategory.category}`}
        onPress={() => navigation.goBack()}
        type="dark"
      />
      <View style={styles.container}>
        {listDoctor.map((item) => {
          return (
            <List
              onPress={() => navigation.navigate('DoctorProfile', item)}
              avatar={{uri: item.data.photo}}
              name={item.data.fullName}
              desc={item.data.gender}
              type="next"
              width={46}
              height={46}
            />
          );
        })}
      </View>
    </View>
  );
};

export default ChooseDoctor;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
  },
});
