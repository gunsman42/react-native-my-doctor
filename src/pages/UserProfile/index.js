import React from 'react';
import {useEffect} from 'react';
import {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {
  IconEditLanguage,
  IconEditProfile,
  IconGiveRate,
  IconHelpCenter,
  ILNullPhoto,
} from '../../assets';
import {Header, List} from '../../components';
import Profile from '../../components/molecules/Prorfile';
import {colors, getData} from '../../utils';
import {Firebase} from '../../config';
import {showMessage} from 'react-native-flash-message';

const UserProfile = ({navigation}) => {
  const [myProfile, setMyProfile] = useState({
    photo: ILNullPhoto,
    fullName: '',
    profession: '',
  });

  useEffect(() => {
    getData('user').then((res) => {
      console.log('data localstorage: ', res);
      const data = res;

      res.photo = {uri: res.photo};

      console.log('data profile: ', data);
      setMyProfile(res);
    });
  }, []);

  const signOut = () => {
    alert('Are you sure want to logout?');
    Firebase.auth()
      .signOut()
      .then((res) => {
        console.log('success Sign Out: ', res);
        navigation.replace('GetStarted');
      })
      .catch((err) => {
        showMessage({
          message: err.message,
          type: 'default',
          icon: 'info',
          backgroundColor: colors.error,
          color: colors.white,
        });
      });
  };

  return (
    <View style={styles.page}>
      <Header title="User Profile" onPress={() => navigation.goBack()} />
      <View style={styles.container}>
        {myProfile.fullName.length > 0 && (
          <Profile
            photo={myProfile.photo}
            name={myProfile.fullName}
            desc={myProfile.profession}
          />
        )}

        <List
          type="next"
          avatar={IconEditProfile}
          name="Edit Profile"
          desc="Last updated yesterday"
          style={styles.icon}
          width={30}
          height={30}
          onPress={() => navigation.navigate('UpdateProfile')}
        />
        <List
          type="next"
          avatar={IconEditLanguage}
          name="Language"
          desc="Current : English"
          style={styles.icon}
          width={30}
          height={30}
        />
        <List
          type="next"
          avatar={IconGiveRate}
          name="Give Us Rate"
          desc="On Google PlayStore"
          style={styles.icon}
          width={30}
          height={30}
        />
        <List
          type="next"
          avatar={IconHelpCenter}
          name="Sign Out"
          desc="Exit to your account"
          style={styles.icon}
          width={30}
          height={30}
          onPress={signOut}
        />
      </View>
    </View>
  );
};

export default UserProfile;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
  },
  container: {
    paddingVertical: 16,
    paddingHorizontal: 16,
  },
  icon: {
    width: 24,
    height: 24,
  },
});
