import React from 'react';
import {useEffect} from 'react';
import {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {ChatItem, Header, InputChat} from '../../components';
import {Firebase} from '../../config';
import {
  colors,
  getChatTiime,
  setDateChat,
  getData,
  showError,
} from '../../utils';

const Chatting = ({navigation, route}) => {
  const dataDoctor = route.params;
  const [chatContent, setChatContent] = useState('');
  const [userData, setUserData] = useState({});
  const [chatData, setChatData] = useState([]);

  useEffect(() => {
    getDataUserFromlocal();
    const chatID = `chatting/${userData.uid}_${dataDoctor.data.uid}`;
    const urlFirebase = `${chatID}/allChat`;

    Firebase.database()
      .ref(urlFirebase)
      .on('value', (snapshot) => {
        console.log('Data Chat from Firebase: ', snapshot.val());
        if (snapshot.val()) {
          const dataSnapshot = snapshot.val();
          const allDataChat = [];

          Object.keys(dataSnapshot).map((item) => {
            const dataChat = dataSnapshot[item];
            const newDataChat = [];
            Object.keys(dataChat).map((key) => {
              newDataChat.push({
                id: key,
                data: dataChat[key],
              });
            });

            allDataChat.push({
              id: item,
              data: newDataChat,
            });
            setChatData(allDataChat);
          });
          console.log('Data Chat from Firebase to Array: ', allDataChat);
        }
      });
  }, [dataDoctor.data.uid, userData.uid]);

  const getDataUserFromlocal = () => {
    getData('user').then((res) => {
      setUserData(res);
    });
  };

  const chatSend = () => {
    const today = new Date();
    const data = {
      sendBy: userData.uid,
      chatDate: new Date().getTime(),
      chatTime: getChatTiime(today),
      chatContent: chatContent,
    };
    console.log('Chat Data For Firebase: ', data);

    const chatID = `${userData.uid}_${dataDoctor.data.uid}`;

    const urlFirebase = `chatting/${chatID}/allChat/${setDateChat(today)}`;
    const urlMessagesUser = `messages/${userData.uid}/${chatID}`;
    const urlMessagesDoctor = `messages/${dataDoctor.data.uid}/${chatID}`;

    const dataHistoryChatforUser = {
      lastContentChat: chatContent,
      lastChatDate: today.getTime(),
      uidPartner: dataDoctor.data.uid,
    };

    const dataHistoryChatforDoctor = {
      lastContentChat: chatContent,
      lastChatDate: today.getTime(),
      uidPartner: userData.uid,
    };

    Firebase.database()
      .ref(urlFirebase)
      .push(data)
      .then(() => {
        setChatContent('');
        //Set History for User
        Firebase.database().ref(urlMessagesUser).set(dataHistoryChatforUser);
        //Set History For Doctor
        Firebase.database()
          .ref(urlMessagesDoctor)
          .set(dataHistoryChatforDoctor);
      })
      .catch((err) => {
        showError(err);
      });
  };

  return (
    <View style={styles.page}>
      <Header
        title={dataDoctor.data.fullName}
        desc={dataDoctor.data.category}
        type="dark-profile"
        image={{uri: dataDoctor.data.photo}}
        onPress={() => navigation.goBack()}
        onPress2={() => navigation.navigate('DoctorProfile')}
      />
      <ScrollView style={styles.container}>
        <View style={styles.wrapper}>
          {chatData.map((chat) => {
            return (
              <View key={chat.id}>
                <Text style={styles.chatDate}>{chat.id}</Text>
                {chat.data.map((item) => {
                  const isMe = item.data.sendBy === userData.uid;
                  return (
                    <ChatItem
                      key={item.id}
                      isMe={isMe}
                      messages={item.data.chatContent}
                      time={item.data.chatTime}
                      photo={isMe ? null : {uri: dataDoctor.data.photo}}
                    />
                  );
                })}
              </View>
            );
          })}
        </View>
      </ScrollView>
      <InputChat
        value={chatContent}
        onChangeText={(value) => setChatContent(value)}
        onButtonPress={chatSend}
        descInput={`Tulis Pesan untuk ${dataDoctor.data.fullName}`}
      />
    </View>
  );
};

export default Chatting;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.white,
    paddingBottom: 26,
  },
  wrapper: {
    paddingHorizontal: 16,
  },
  container: {
    flex: 1,
  },
  chatDate: {
    textAlign: 'center',
    paddingVertical: 20,
    color: colors.text.secondary,
  },
});
