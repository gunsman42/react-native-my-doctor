import React, {useEffect, useState} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {Gap, List} from '../../components';
import {Firebase} from '../../config';
import {colors, fonts, getData} from '../../utils';

export default function Messages({navigation}) {
  const [userData, setUserData] = useState({});
  const [historyChat, setHistoryChat] = useState([]);

  const getDataUserFromlocal = () => {
    getData('user').then((res) => {
      setUserData(res);
    });
  };

  useEffect(() => {
    const rootDB = Firebase.database().ref();
    const urlHistory = `messages/${userData.uid}`;
    const messagesDB = rootDB.child(urlHistory);
    getDataUserFromlocal();
    messagesDB.on('value', async (snapshot) => {
      if (snapshot.val()) {
        const oldData = snapshot.val();
        const data = [];
        const promises = await Object.keys(oldData).map(async (key) => {
          const urlUidDoctor = `doctors/${oldData[key].uidPartner}`;
          const detailDoctor = await rootDB.child(urlUidDoctor).once('value');
          console.log('Detail Doctor: ', detailDoctor.val());
          data.push({
            id: key,
            detailDoctor: detailDoctor.val(),
            ...oldData[key],
          });
        });
        await Promise.all(promises);

        console.log('New Data Chat : ', data);
        setHistoryChat(data);
      }
    });
  }, [userData.uid]);

  return (
    <View style={styles.page}>
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.wrapper}>
            <Gap height={16} />
            <Text style={styles.title}>Messages</Text>
          </View>
          {historyChat.map((chat) => {
            const dataDoctor = {
              id: chat.detailDoctor.uid,
              data: chat.detailDoctor,
            };
            return (
              <List
                key={chat.id}
                avatar={{uri: chat.detailDoctor.photo}}
                name={chat.detailDoctor.fullName}
                desc={chat.lastContentChat}
                width={46}
                height={46}
                onPress={() => navigation.navigate('Chatting', dataDoctor)}
              />
            );
          })}
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.secondary,
    flex: 1,
  },
  container: {
    backgroundColor: colors.white,
    flex: 1,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  wrapper: {
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 20,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
  },
});
