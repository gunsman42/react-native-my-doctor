import React, {useState} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {showMessage} from 'react-native-flash-message';
import {TouchableOpacity} from 'react-native-gesture-handler';
//Image Picker
import {launchImageLibrary} from 'react-native-image-picker';
import {IconAddPhoto, IconRemovePhoto, ILNullPhoto} from '../../assets';
import {Button, Gap, Header, Link} from '../../components';
import {Firebase} from '../../config';
import {colors, fonts, showError, storeData} from '../../utils';

export default function UploadPhoto({navigation, route}) {
  const {fullName, profession, uid} = route.params;
  const [hasPhoto, setHasPhoto] = useState(false);
  const [photo, setPhoto] = useState(ILNullPhoto);
  const [photoForDB, setPhotoForDB] = useState('');

  const options = {
    includeBase64: true,
    quality: 0.5,
    maxWidth: 200,
    maxHeight: 200,
  };
  const getImageFromGallery = () => {
    launchImageLibrary(options, (response) => {
      if (response.didCancel || response.error) {
        showError("Oops, you have'nt choose any photos");
      } else {
        const source = {uri: response.uri};
        setPhotoForDB(`data:${response.type};base64, ${response.base64}`);
        setPhoto(source);
        setHasPhoto(true);
        console.log('response Upload photo: ', response);
      }
    });
  };
  const uploadAndContinue = () => {
    Firebase.database()
      .ref('users/' + uid + '/')
      .update({photo: photoForDB});

    const data = route.params;
    data.photo = photoForDB;

    storeData('user', data);

    navigation.replace('MainApp');
  };
  return (
    <View style={styles.page}>
      <Header
        title="Upload Photo"
        icon="back-dark"
        onPress={() => navigation.goBack()}
      />
      <View style={styles.container}>
        <View style={styles.avatarContainer}>
          <TouchableOpacity
            style={styles.avatarWrapper}
            onPress={getImageFromGallery}>
            <Image source={photo} style={styles.avatar} />
            {!hasPhoto && <IconAddPhoto style={styles.iconAvatar} />}
            {hasPhoto && <IconRemovePhoto style={styles.iconAvatar} />}
          </TouchableOpacity>
          <Gap height={26} />
          <Text style={styles.name}>{fullName}</Text>
          <Gap height={4} />
          <Text style={styles.job}>{profession}</Text>
        </View>
        <View>
          <Button
            disable={!hasPhoto}
            title="Upload and Continue"
            onPress={uploadAndContinue}
          />

          <Gap height={30} />
          <Link
            link="Skip for this"
            align="center"
            size={16}
            onPress={() => navigation.replace('MainApp')}
          />
          <Gap height={64} />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
  },
  container: {
    paddingHorizontal: 40,
    backgroundColor: colors.white,
    flex: 1,
    justifyContent: 'space-between',
  },
  avatarContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarWrapper: {
    height: 130,
    width: 130,
    borderWidth: 1,
    borderRadius: 130,
    borderColor: colors.border,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  avatar: {
    height: 110,
    width: 110,
    borderRadius: 110 / 2,
  },
  iconAvatar: {
    position: 'absolute',
    right: 0,
    bottom: 8,
  },
  name: {
    fontFamily: fonts.primary[600],
    fontSize: 24,
    color: colors.text.primary,
    textAlign: 'center',
  },
  job: {
    fontFamily: fonts.primary.normal,
    fontSize: 18,
    color: colors.text.secondary,
    textAlign: 'center',
  },
});
