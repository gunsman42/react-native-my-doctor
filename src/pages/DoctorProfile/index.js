import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Button} from '../../components/atoms';
import {Header, List} from '../../components/molecules';
import Profile from '../../components/molecules/Prorfile';
import {colors} from '../../utils';

const DoctorProfile = ({navigation, route}) => {
  const dataDoctor = route.params;
  return (
    <View style={styles.page}>
      <Header title="Profile" onPress={() => navigation.goBack()} />
      <Profile
        photo={{uri: dataDoctor.data.photo}}
        name={dataDoctor.data.fullName}
        desc={dataDoctor.data.category}
      />
      <View style={styles.listWrap}>
        <List name="Alumnus" desc={dataDoctor.data.university} />
        <List name="Tempat Praktik" desc={dataDoctor.data.hospital_address} />
        <List name="No. STR" desc={dataDoctor.data.str_number} />
      </View>
      <View style={styles.button}>
        <Button
          title="Start Consultation"
          onPress={() => navigation.navigate('Chatting', dataDoctor)}
        />
      </View>
    </View>
  );
};

export default DoctorProfile;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
    flex: 1,
    justifyContent: 'space-between',
  },
  button: {
    paddingHorizontal: 40,
    paddingBottom: 40,
  },
});
