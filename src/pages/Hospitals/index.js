import React, {useState} from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import {ILCoverHospital} from '../../assets';
import {HospitalPlace} from '../../components';
import {Firebase} from '../../config';
import {colors, fonts, showError} from '../../utils';

export default function Hospitals() {
  const [hospital, setHospital] = useState([]);

  Firebase.database()
    .ref('hospital/')
    .once('value')
    .then((res) => {
      setHospital(res.val());
    })
    .catch((err) => {
      showError(err.message);
    });

  return (
    <View style={styles.page}>
      <View style={styles.wrapper}>
        <ImageBackground source={ILCoverHospital} style={styles.cover}>
          <View style={styles.textWrapper}>
            <Text style={styles.title}>Nearby Hospitals</Text>
            <Text style={styles.desc}>3 Available</Text>
          </View>
        </ImageBackground>
      </View>
      <View style={styles.container}>
        {hospital.map((item) => {
          return (
            <HospitalPlace
              key={item.id}
              type={item.hospitalType}
              name={item.name}
              address={item.address}
              picture={item.image}
            />
          );
        })}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.secondary,
    flex: 1,
  },
  container: {
    backgroundColor: colors.white,
    flex: 1,
    borderRadius: 20,
    marginTop: -30,
    paddingTop: 30,
  },
  wrapper: {
    height: 250,
  },
  textWrapper: {
    alignItems: 'center',
    marginTop: 30,
  },
  cover: {
    flex: 1,
    height: 240,
  },
  title: {
    fontFamily: fonts.primary[600],
    fontSize: 20,
    color: colors.white,
  },
  desc: {
    fontFamily: fonts.primary[300],
    fontSize: 14,
    color: colors.white,
  },
});
