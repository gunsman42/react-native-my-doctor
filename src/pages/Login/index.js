import React from 'react';
import {ScrollView, StyleSheet, Text} from 'react-native';
import {useDispatch} from 'react-redux';
import {ILLogo} from '../../assets';
import {Button, Gap, Input, Link} from '../../components';
import {Firebase} from '../../config';
import {colors, fonts, showError, showSuccess, storeData} from '../../utils';
import useForm from '../../utils/useForm';

export default function Login({navigation}) {
  const [form, setForm] = useForm({
    email: '',
    password: '',
  });

  const dispatch = useDispatch();

  const login = () => {
    // console.log('form: ', form);
    dispatch({type: 'SET_LOADING', value: true});
    Firebase.auth()
      .signInWithEmailAndPassword(form.email, form.password)
      .then((res) => {
        dispatch({type: 'SET_LOADING', value: false});

        Firebase.database()
          .ref(`users/${res.user.uid}/`)
          .once('value')
          .then((resDB) => {
            console.log('data user: ', resDB.val());

            if (resDB.val()) {
              showSuccess('Login Success!');
              const data = resDB.val();
              // data.photo = resDB.val().photoForDB;

              storeData('user', data);
              navigation.replace('MainApp');
            }
          });

        console.log('Login Success: ', res);
      })
      .catch((err) => {
        dispatch({type: 'SET_LOADING', value: false});
        setForm('reset');
        // const errorMessage = err.message;
        showError('Incorrect email or password , please try again');
        console.log('Login Error: ', err);
      });
  };

  return (
    <ScrollView style={styles.page} showsVerticalScrollIndicator={false}>
      <ILLogo />
      <Text style={styles.title}>Masuk dan mulai berkonsultasi</Text>
      <Input
        label="Email Address"
        value={form.email}
        onChangeText={(value) => setForm('email', value)}
      />
      <Gap height={24} />
      <Input
        label="Password"
        password
        value={form.password}
        onChangeText={(value) => setForm('password', value)}
      />
      <Gap height={10} />
      <Link link="Forgot My Password" size={12} />
      <Gap height={40} />
      <Button title="Sign In" onPress={login} />
      <Gap height={30} />
      <Link
        link="Create New Account"
        size={16}
        align="center"
        onPress={() => navigation.navigate('Register')}
      />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  page: {
    padding: 40,
    backgroundColor: colors.white,
    flex: 1,
  },
  title: {
    fontFamily: fonts.primary[600],
    fontSize: 20,
    color: colors.text.primary,
    marginVertical: 40,
    maxWidth: 154,
  },
});
