import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {ILNullPhoto} from '../../assets';
import {Gap} from '../../components/atoms';
import {
  DoctorCategory,
  HomeProfile,
  RatedDoctor,
} from '../../components/molecules';
import NewsItem from '../../components/molecules/NewsItem';
import {Firebase} from '../../config';
import {colors, fonts, getData, showError} from '../../utils';

export default function Doctor({navigation}) {
  const [profile, setProfile] = useState({
    photo: ILNullPhoto,
    fullName: '',
    profession: '',
  });

  const [news, setNews] = useState([]);
  const [docCategory, setDocCategory] = useState([]);
  const [topRatedDoctor, setTopRatedDoctor] = useState([]);

  useEffect(() => {
    getDoctorCategory();
    getTopRatedDoctor();
    getNews();

    navigation.addListener('focus', () => {
      getData('user').then((res) => {
        const data = res;
        data.photo = res?.photo?.length > 1 ? {uri: res.photo} : ILNullPhoto;
        setProfile(res);
      });
    });
  }, [navigation]);

  const getDoctorCategory = () => {
    Firebase.database()
      .ref('doc_category/')
      .once('value')
      .then((res) => {
        if (res.val()) {
          const data = res.val();
          const filterData = data.filter((el) => el !== null);
          console.log('category Doc Hasil Filter: ', filterData);
          setDocCategory(filterData);
        }
      })
      .catch((err) => {
        showError(err.message);
      });
  };

  const getNews = () => {
    Firebase.database()
      .ref('news/')
      .once('value')
      .then((res) => {
        if (res.val()) {
          const data = res.val();
          const filterData = data.filter((el) => el !== null);
          console.log('News Data : ', filterData);
          setNews(filterData);
        }
      })
      .catch((err) => {
        showError(err.message);
      });
  };

  const getTopRatedDoctor = () => {
    Firebase.database()
      .ref('doctors/')
      .orderByChild('rate')
      .limitToLast(3)
      .once('value')
      .then((res) => {
        console.log('Top Rate Doctor: ', res.val());
        if (res.val()) {
          const oldData = res.val();
          const data = [];
          Object.keys(oldData).map((key) => {
            data.push({
              id: key,
              data: oldData[key],
            });
          });
          console.log('data rate Doctor: ', data);
          setTopRatedDoctor(data);
        }
      })
      .catch((err) => {
        showError(err.message);
      });
  };

  return (
    <View style={styles.page}>
      <View style={styles.content}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Gap height={16} />
          <View style={styles.wrapper}>
            <HomeProfile
              profile={profile}
              onPress={() => navigation.navigate('UserProfile')}
            />
            <Gap height={16} />
            <Text style={styles.categoryTitle}>Mau konsultasi</Text>
            <Text style={styles.categoryTitle}>dengan siapa hari ini?</Text>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              style={styles.DoctorCategoryWrap}>
              <Gap width={16} />
              {docCategory.map((item) => {
                return (
                  <DoctorCategory
                    key={item.id}
                    category={item.category}
                    onPress={() => navigation.navigate('ChooseDoctor', item)}
                  />
                );
              })}
            </ScrollView>
            <Gap height={30} />
            <Text style={styles.relatedDoctorTitle}>Top Rated Doctors</Text>
            <Gap height={16} />
            {topRatedDoctor.map((item) => {
              return (
                <RatedDoctor
                  key={item.id}
                  name={item.data.fullName}
                  desc={item.data.category}
                  avatar={{uri: item.data.photo}}
                  rate={item.data.rate}
                  onPress={() => navigation.navigate('DoctorProfile', item)}
                />
              );
            })}

            <View style={styles.titleWrap}>
              <Text style={styles.newsTitle}>Good News</Text>
            </View>
          </View>
          {news.map((item) => {
            return (
              <NewsItem
                key={item.id}
                title={item.title}
                date={item.date}
                image={item.image}
              />
            );
          })}
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.secondary,
    flex: 1,
  },
  content: {
    backgroundColor: colors.white,
    flex: 1,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  wrapper: {
    paddingHorizontal: 16,
  },
  categoryTitle: {
    fontFamily: fonts.primary[600],
    fontSize: 20,
    color: colors.text.primary,
  },
  DoctorCategoryWrap: {
    flexDirection: 'row',
    marginHorizontal: -16,
  },
  relatedDoctorTitle: {
    fontFamily: fonts.primary[700],
    fontSize: 16,
    color: colors.text.primary,
  },
  newsTitle: {
    fontFamily: fonts.primary[700],
    fontSize: 16,
    color: colors.text.primary,
  },
  titleWrap: {
    paddingBottom: 16,
  },
});
