import React from 'react';
import {StyleSheet, View} from 'react-native';
import {colors} from '../../../utils';

const LineSeperate = () => {
  return (
    <View style={styles.container}>
      <View style={styles.line} />
    </View>
  );
};

export default LineSeperate;

const styles = StyleSheet.create({
  container: {
    marginVertical: 16,
  },
  line: {
    borderWidth: 1,
    borderColor: colors.border,
    flex: 1,
  },
});
