import React from 'react';
import {StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {IconSendDark, IconSendLight} from '../../../assets';
import {colors} from '../../../utils';

const BtnIconSend = ({disable, onPress}) => {
  if (disable) {
    return (
      <TouchableOpacity style={styles.wrapper(disable)} onPress={onPress}>
        <IconSendDark />
      </TouchableOpacity>
    );
  }

  return (
    <TouchableOpacity style={styles.wrapper(disable)} onPress={onPress}>
      <IconSendLight />
    </TouchableOpacity>
  );
};

export default BtnIconSend;

const styles = StyleSheet.create({
  wrapper: (disable) => ({
    width: 45,
    height: 45,
    backgroundColor: disable ? colors.input : colors.send,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  }),
  icon: {
    width: 34,
    height: 34,
    left: 3,
    bottom: 2,
  },
});
