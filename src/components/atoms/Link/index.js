import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {colors, fonts} from '../../../utils';

export default function Link({link, size, align, onPress}) {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={styles.link(size, align)}>{link}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  link: (size, align) => ({
    fontSize: size,
    textDecorationLine: 'underline',
    color: colors.text.secondary,
    fontFamily: fonts.primary[600],
    textAlign: align,
  }),
});
