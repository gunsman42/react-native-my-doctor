import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';

const SuccessInfo = () => {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.text}>Register Success</Text>
    </View>
  );
};

export default SuccessInfo;

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#26C281',
    width: '100%',
    height: 30,
    justifyContent: 'center',
  },
  text: {
    textAlign: 'center',
    fontFamily: fonts.primary[600],
    fontSize: 14,
    color: colors.white,
  },
});
