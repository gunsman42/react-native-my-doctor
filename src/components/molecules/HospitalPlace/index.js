import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';

const HospitalPlace = ({picture, type, name, address}) => {
  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <Image source={{uri: picture}} style={styles.image} />
        <View style={styles.textWrapper}>
          <Text style={styles.title}>{type}</Text>
          <Text style={styles.title}>{name}</Text>
          <Text style={styles.desc}>{address}</Text>
        </View>
      </View>
    </View>
  );
};

export default HospitalPlace;

const styles = StyleSheet.create({
  container: {flex: 0},
  wrapper: {
    paddingHorizontal: 16,
    flexDirection: 'row',
    paddingVertical: 16,
    borderBottomWidth: 1,
    borderBottomColor: colors.border,
  },
  image: {
    width: 80,
    height: 62,
    borderRadius: 11,
  },
  textWrapper: {
    justifyContent: 'center',
    marginLeft: 16,
  },
  title: {
    fontFamily: fonts.primary.normal,
    fontSize: 16,
    color: colors.text.primary,
    maxWidth: 200,
  },
  desc: {
    fontFamily: fonts.primary[300],
    fontSize: 16,
    color: colors.text.secondary,
    maxWidth: 300,
  },
});
