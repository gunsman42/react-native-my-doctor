import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {colors, fonts} from '../../../utils';
import {Button, Gap} from '../../atoms';

const DarkProfile = ({onPress, title, desc, onPress2, image}) => {
  return (
    <View style={styles.container}>
      <Button type="icon-only" icon="back-light" onPress={onPress} />
      <TouchableOpacity style={styles.wrapper} onPress={onPress2}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.desc}>{desc}</Text>
      </TouchableOpacity>
      <View style={styles.avatarWrap}>
        <Image source={image} style={styles.image} />
        <Gap height={10} />
      </View>
    </View>
  );
};

export default DarkProfile;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.secondary,
    paddingVertical: 2,
    paddingLeft: 20,
    paddingRight: 16,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  image: {
    width: 38,
    height: 38,
    borderRadius: 38 / 2,
  },
  wrapper: {},
  title: {
    fontFamily: fonts.primary[600],
    fontSize: 16,
    color: colors.white,
    textAlign: 'center',
    paddingBottom: 2,
  },
  desc: {
    fontFamily: fonts.primary[300],
    fontSize: 14,
    color: colors.chat,
    textAlign: 'center',
  },
  avatarWrap: {
    justifyContent: 'center',
  },
});
