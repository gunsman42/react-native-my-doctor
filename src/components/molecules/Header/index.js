import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';
import {Button, Gap} from '../../atoms';
import DarkProfile from './DarkProfile';

export default function Header({title, onPress, onPress2, type, desc, image}) {
  if (type === 'dark-profile') {
    return (
      <DarkProfile
        onPress2={onPress2}
        title={title}
        desc={desc}
        onPress={onPress}
        image={image}
      />
    );
  }
  return (
    <View style={styles.container(type)}>
      <Button
        type="icon-only"
        icon={type === 'dark' ? 'back-light' : 'back-dark'}
        onPress={onPress}
      />
      <Text style={styles.text(type)}>{title}</Text>

      <Gap width={24} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: (type) => ({
    paddingHorizontal: 16,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: type === 'dark' ? colors.secondary : colors.white,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  }),
  text: (type) => ({
    fontFamily: fonts.primary[600],
    fontSize: 18,
    textAlign: 'center',
    flex: 1,
    color: type === 'dark' ? colors.white : colors.text.primary,
  }),
});
