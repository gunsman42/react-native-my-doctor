import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {
  IconDokterAnak,
  IconDokterObat,
  IconDokterPsikiater,
  IconDokterUmum,
} from '../../../assets';
import {colors, fonts} from '../../../utils';

const DoctorCategory = ({icon, category, onPress}) => {
  const IconDokter = () => {
    if (category === 'Dokter Umum') {
      return <IconDokterUmum />;
    }
    if (category === 'Dokter Psikiater') {
      return <IconDokterPsikiater />;
    }
    if (category === 'Dokter Obat') {
      return <IconDokterObat />;
    }
    if (category === 'Dokter Anak') {
      return <IconDokterAnak />;
    }
    return <IconDokterUmum />;
  };
  return (
    <View style={styles.page}>
      <TouchableOpacity style={styles.container} onPress={onPress}>
        <IconDokter />
        <Text style={styles.listDoctorTitle}>Saya Butuh</Text>
        <Text style={styles.listDoctorDesc}>{category}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default DoctorCategory;

const styles = StyleSheet.create({
  page: {
    marginTop: 16,
  },
  container: {
    borderWidth: 0,
    borderRadius: 10,
    height: 130,
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.backgroundList,
    marginRight: 10,
  },
  listDoctorTitle: {
    fontSize: 14,
    fontFamily: fonts.primary[300],
    color: colors.text.primary,
    marginTop: 20,
  },
  listDoctorDesc: {
    fontSize: 12,
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
  },
});
