import React from 'react';
import {StyleSheet, View} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import {colors, fonts} from '../../../utils';
import {Button} from '../../atoms';

const InputChat = ({value, onChangeText, onButtonPress, descInput}) => {
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder={descInput}
        value={value}
        onChangeText={onChangeText}
      />
      <View>
        <Button
          type="btn-icon-send"
          onPress={onButtonPress}
          disable={value.length < 1}
        />
      </View>
    </View>
  );
};

export default InputChat;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.white,
  },
  input: {
    borderRadius: 10,
    width: 300,
    height: 45,
    backgroundColor: colors.input,
    padding: 14,
    marginRight: 10,
    fontSize: 14,
    fontFamily: fonts.primary.normal,
  },
});
