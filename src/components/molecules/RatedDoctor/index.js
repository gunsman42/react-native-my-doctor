import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {IconOnline, StarIcon} from '../../../assets';
import {colors, fonts} from '../../../utils';

const RatedDoctor = ({onPress, avatar, name, desc, online, rate}) => {
  const rateStar = () => {
    if (rate === 1) {
      return (
        <View style={styles.starWrapper}>
          <StarIcon />
        </View>
      );
    }
    if (rate === 2) {
      return (
        <View style={styles.starWrapper}>
          <StarIcon />
          <StarIcon />
        </View>
      );
    }
    if (rate === 3) {
      return (
        <View style={styles.starWrapper}>
          <StarIcon />
          <StarIcon />
          <StarIcon />
        </View>
      );
    }
    if (rate === 4) {
      return (
        <View style={styles.starWrapper}>
          <StarIcon />
          <StarIcon />
          <StarIcon />
          <StarIcon />
        </View>
      );
    }
    if (rate === 5) {
      return (
        <View style={styles.starWrapper}>
          <StarIcon />
          <StarIcon />
          <StarIcon />
          <StarIcon />
          <StarIcon />
        </View>
      );
    }
  };

  return (
    <View>
      <TouchableOpacity style={styles.container} onPress={onPress}>
        <Image source={avatar} style={styles.avatar} />
        {online && <Image source={IconOnline} style={styles.icon} />}
        <View style={styles.profile}>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.category}>{desc}</Text>
        </View>
        {rate && rateStar()}
      </TouchableOpacity>
    </View>
  );
};

export default RatedDoctor;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 20,
  },
  profile: {
    flex: 1,
  },
  avatar: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
    marginRight: 12,
    position: 'relative',
  },
  icon: {
    width: 14,
    height: 14,
    position: 'absolute',
    bottom: 20,
    left: 38,
  },
  starWrapper: {
    flexDirection: 'row',
  },
  name: {
    fontFamily: fonts.primary[600],
    fontSize: 16,
    color: colors.text.primary,
  },
  category: {
    fontFamily: fonts.primary.normal,
    fontSize: 14,
    color: colors.text.secondary,
    marginTop: 2,
    textTransform: 'capitalize',
  },
});
