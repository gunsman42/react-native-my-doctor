import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {IconNext} from '../../../assets';
import {colors, fonts} from '../../../utils';

const List = ({avatar, name, desc, type, onPress, width, height}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.wrapperMessages} onPress={onPress}>
        <Image source={avatar} style={styles.avatar(width, height)} />
        <View style={styles.wrapperChat}>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.messages}>{desc}</Text>
        </View>
        {type === 'next' && <IconNext />}
      </TouchableOpacity>
    </View>
  );
};

export default List;

const styles = StyleSheet.create({
  container: {
    paddingTop: 16,
    paddingBottom: 16,
    borderBottomWidth: 1,
    borderBottomColor: colors.border,
    justifyContent: 'space-between',
  },
  wrapperMessages: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  avatar: (width, height) => ({
    height: width,
    width: height,
    borderRadius: 46 / 2,
  }),
  wrapperChat: {
    marginLeft: 12,
    flex: 1,
  },
  name: {
    fontSize: 16,
    fontFamily: fonts.primary.normal,
    color: colors.text.primary,
  },
  messages: {
    fontSize: 14,
    fontFamily: fonts.primary.normal,
    color: colors.text.secondary,
    maxWidth: 400,
    textTransform: 'capitalize',
  },
});
