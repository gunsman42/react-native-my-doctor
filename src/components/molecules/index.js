import Header from './Header';
import BottomNavigator from './BottomNavigator';
import HomeProfile from './HomeProfile';
import DoctorCategory from './DoctorCategory';
import RatedDoctor from './RatedDoctor';
import News from './NewsItem';
import List from './List';
import HospitalPlace from './HospitalPlace';
import ChatItem from './ChatItem';
import InputChat from './InputChat';
import Loading from './Loading';
import SuccessInfo from './SuccessInfo';

export {
  Header,
  BottomNavigator,
  HomeProfile,
  DoctorCategory,
  RatedDoctor,
  News,
  List,
  HospitalPlace,
  ChatItem,
  InputChat,
  Loading,
  SuccessInfo,
};
