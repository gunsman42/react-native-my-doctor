import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';

const NewsItem = ({title, date, image}) => {
  return (
    <View styles={styles.container}>
      <View style={styles.wrapper}>
        <View style={styles.textWrap}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.time}>{date}</Text>
        </View>
        <View>
          <Image source={{uri: image}} style={styles.image} />
        </View>
      </View>
    </View>
  );
};

export default NewsItem;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    borderBottomWidth: 1,
    borderBottomColor: colors.border,
    marginBottom: 16,
  },
  textWrap: {
    marginBottom: 16,
  },
  title: {
    maxWidth: 280,
    fontFamily: fonts.primary[600],
    fontSize: 16,
    color: colors.text.primary,
    paddingBottom: 4,
  },
  time: {
    fontFamily: fonts.primary[300],
    fontSize: 12,
    color: colors.text.secondary,
  },
  image: {
    width: 80,
    height: 60,
  },
});
