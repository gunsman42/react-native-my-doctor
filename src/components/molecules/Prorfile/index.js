import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {IconRemovePhoto} from '../../../assets';
import {colors, fonts} from '../../../utils';

const Profile = ({isRemove, name, desc, photo, onPress}) => {
  return (
    <View style={styles.container}>
      {!isRemove && (
        <View style={styles.avatarWrap}>
          <Image source={photo} style={styles.image} />
          {isRemove && <IconRemovePhoto style={styles.icon} />}
        </View>
      )}
      {isRemove && (
        <TouchableOpacity style={styles.avatarWrap} onPress={onPress}>
          <Image source={photo} style={styles.image} />
          {isRemove && <IconRemovePhoto style={styles.icon} />}
        </TouchableOpacity>
      )}
      {name && (
        <View style={styles.profile}>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.desc}>{desc}</Text>
        </View>
      )}
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  image: {
    width: 110,
    height: 110,
    borderRadius: 110 / 2,
  },
  avatarWrap: {
    width: 130,
    height: 130,
    borderWidth: 1,
    borderColor: colors.primary,
    borderRadius: 130 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  profile: {
    paddingTop: 16,
    paddingBottom: 16,
  },
  name: {
    fontFamily: fonts.primary[600],
    fontSize: 20,
    color: colors.text.primary,
    textAlign: 'center',
  },
  desc: {
    fontFamily: fonts.primary[300],
    fontSize: 16,
    color: colors.text.secondary,
    textAlign: 'center',
    textTransform: 'capitalize',
  },
  icon: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
});
