import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {colors, fonts} from '../../../utils';

const HomeProfile = ({onPress, profile}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.profileContainer} onPress={onPress}>
        <Image source={profile.photo} style={styles.ProfileImage} />
      </View>
      <View style={styles.textContainer}>
        <Text style={styles.profileName}>{profile.fullName}</Text>
        <Text style={styles.profileDesc}>{profile.profession}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default HomeProfile;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  wrap: {
    flexDirection: 'row',
  },
  profileContainer: {
    borderWidth: 1,
    borderRadius: 46,
    padding: 2,
    borderColor: colors.primary,
  },
  ProfileImage: {
    width: 46,
    height: 46,
    borderRadius: 46,
  },
  textContainer: {
    justifyContent: 'center',
    marginLeft: 12,
  },
  profileName: {
    color: colors.text.primary,
    fontFamily: fonts.primary[600],
    fontSize: 16,
    textTransform: 'capitalize',
  },
  profileDesc: {
    color: colors.text.secondary,
    fontFamily: fonts.primary.normal,
    fontSize: 14,
    textTransform: 'capitalize',
  },
});
