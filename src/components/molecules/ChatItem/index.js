import React from 'react';
import IsMe from './IsMe';
import Other from './Other';

const ChatItem = ({isMe, messages, time, photo}) => {
  if (isMe) {
    return <IsMe messages={messages} time={time} />;
  }
  return <Other messages={messages} time={time} photo={photo} />;
};

export default ChatItem;
