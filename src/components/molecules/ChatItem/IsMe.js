import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';

const IsMe = ({messages, time}) => {
  return (
    <View style={styles.wrap}>
      <View style={styles.container}>
        <Text style={styles.text}>{messages}</Text>
      </View>
      <View style={styles.timeWrap}>
        <Text style={styles.time}>{time}</Text>
      </View>
    </View>
  );
};

export default IsMe;

const styles = StyleSheet.create({
  wrap: {
    alignItems: 'flex-end',
    paddingBottom: 16,
  },
  container: {
    backgroundColor: colors.message.light,
    maxWidth: '70%',
    justifyContent: 'center',
    padding: 12,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
  },
  text: {
    fontSize: 14,
    fontFamily: fonts.primary.normal,
    color: colors.text.primary,
  },
  timeWrap: {
    paddingTop: 8,
  },
  time: {
    color: colors.text.secondary,
    fontFamily: fonts.primary[300],
    fontSize: 11,
  },
});
