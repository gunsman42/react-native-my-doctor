import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utils';

const Other = ({time, messages, photo}) => {
  return (
    <View style={styles.wrap}>
      <Image source={photo} style={styles.avatar} />
      <View>
        <View style={styles.container}>
          <Text style={styles.text}>{messages}</Text>
        </View>
        <View style={styles.timeWrap}>
          <Text style={styles.time}>{time}</Text>
        </View>
      </View>
    </View>
  );
};

export default Other;

const styles = StyleSheet.create({
  wrap: {
    alignItems: 'flex-end',
    paddingBottom: 16,
    flexDirection: 'row',
  },
  avatar: {
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
  },
  container: {
    backgroundColor: colors.primary,
    maxWidth: '100%',
    minWidth: '35%',
    justifyContent: 'center',
    padding: 12,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    paddingLeft: 16,
    marginLeft: 12,
  },
  text: {
    fontSize: 14,
    fontFamily: fonts.primary.normal,
    color: colors.white,
  },
  timeWrap: {
    paddingTop: 8,
    marginLeft: 12,
  },
  time: {
    color: colors.text.secondary,
    fontFamily: fonts.primary[300],
    fontSize: 11,
  },
});
