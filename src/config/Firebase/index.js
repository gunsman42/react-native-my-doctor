import firebase from 'firebase';

firebase.initializeApp({
  apiKey: 'AIzaSyADOTg19cWJdXbUFGcQT01H9JHhQEn1YGE',
  authDomain: 'my-doctor-50e7a.firebaseapp.com',
  projectId: 'my-doctor-50e7a',
  storageBucket: 'my-doctor-50e7a.appspot.com',
  messagingSenderId: '619058478816',
  appId: '1:619058478816:web:bb12bf099882ff01b19a85',
});

const Firebase = firebase;

export default Firebase;
