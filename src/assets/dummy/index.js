import DummyProfileImage from './user.jpg';
import DummyDoctorA from './dummy-doctor-1.png';
import DummyDoctorB from './dummy-doctor-2.png';
import DummyDoctorC from './dummy-doctor-3.png';
import DummyDoctorD from './dummy-doctor-4.png';
import DummyDoctorE from './dummy-doctor-5.png';
import DummyDoctorF from './dummy-doctor-6.png';
import DummyDoctorG from './dummy-doctor-7.png';
import DummyDoctorH from './dummy-doctor-8.png';
import StarIcon from './star-rate.svg';
import News1 from './newsA.png';
import News2 from './newsB.png';
import News3 from './newsC.png';
import DummyProfileImage2 from './user-2.png';

export {
  DummyProfileImage,
  DummyDoctorA,
  DummyDoctorB,
  DummyDoctorC,
  DummyDoctorD,
  DummyDoctorE,
  DummyDoctorF,
  DummyDoctorG,
  DummyDoctorH,
  StarIcon,
  News1,
  News2,
  News3,
  DummyProfileImage2,
};
