import IconBackDark from './ic-back-dark.svg';
import IconAddPhoto from './ic-add-photo.svg';
import IconRemovePhoto from './ic-remove-photo.svg';
import IconDoctorActive from './icon-doctor-active.svg';
import IconDoctor from './icon-doctor.svg';
import IconMessagesActive from './icon-messages-active.svg';
import IconMessages from './icon-messages.svg';
import IconHospitalsActive from './icon-hospitals-active.svg';
import IconHospitals from './icon-hospitals.svg';
import IconDokterAnak from './icon_dokter_anak.svg';
import IconDokterObat from './icon_dokter_obat.svg';
import IconDokterPsikiater from './icon_dokter_psikiater.svg';
import IconDokterUmum from './icon_dokter_umum.svg';
import IconNext from './icon-next.svg';
import IconBackLight from './icon-back-light.svg';
import IconSendDark from './icon-send-dark.svg';
import IconSendLight from './icon-send-light.svg';
import IconEditProfile from './icon-edit-profile.png';
import IconEditLanguage from './icon-edit-language.png';
import IconGiveRate from './icon-giverate.png';
import IconHelpCenter from './icon-helpcenter.png';
import IconOnline from './icon-online.png';

export {
  IconBackDark,
  IconAddPhoto,
  IconRemovePhoto,
  IconDoctorActive,
  IconDoctor,
  IconMessagesActive,
  IconMessages,
  IconHospitalsActive,
  IconHospitals,
  IconDokterAnak,
  IconDokterObat,
  IconDokterPsikiater,
  IconDokterUmum,
  IconNext,
  IconBackLight,
  IconSendDark,
  IconSendLight,
  IconEditProfile,
  IconEditLanguage,
  IconGiveRate,
  IconHelpCenter,
  IconOnline,
};
