import ILLogo from './logo.svg';
import ILGetStarted from './get-started.png';
import ILNullPhoto from './null-photo.png';
import ILCoverHospital from './cover-hospital.png';
import ILHospital1 from './hospital-1.png';
import ILHospital2 from './hospital-2.png';
import ILHospital3 from './hospital-3.png';

export {
  ILLogo,
  ILGetStarted,
  ILNullPhoto,
  ILCoverHospital,
  ILHospital1,
  ILHospital2,
  ILHospital3,
};
